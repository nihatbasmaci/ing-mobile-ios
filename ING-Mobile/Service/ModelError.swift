//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//


class ModelError: Codable
{
    var Code:String?
    var UserFriendlyMessage:String = "Bilinmeyen bir hata oluştu."
    var DeveloperMessage:String?
}
