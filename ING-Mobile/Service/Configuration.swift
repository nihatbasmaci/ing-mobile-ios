//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//

struct Configuration
{
    var baseURL: String = ""
    var clientId: String = ""
    var baseImageURL: String = "https://image.tmdb.org/t/p/w200/"
}
