//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//


import UIKit

struct APILookUp
{
    static func getPopular() -> URL
    {
        return URL(string: Manager.shared.configuration.baseURL + "3/movie/popular")!
    }
    
    static func getDetail(movieID:Int) -> URL
    {
        return URL(string: Manager.shared.configuration.baseURL + "3/movie/\(movieID.description)")!
    }
}

enum RequestMethod: String
{
    case GET
    case POST
}

enum RequestConttentType: String
{
    case JSON
    case URLEncoded
}

class RequestManager: NSObject
{
    static func post(url: URL, parameters: [String:Any]?, success: @escaping (Data) -> (Void), failure: @escaping (ModelError) -> (Void))
    {
        self.request(method: .POST, url: url, parameters: parameters, contentType: .JSON, includeSession: true, success: success, failure: failure)
    }
    
    static func get(url: URL, parameters: [String:String]?, success: @escaping (Data) -> (Void), failure: @escaping (ModelError) -> (Void))
    {
        self.request(method: .GET, url: url, parameters: parameters, contentType: .URLEncoded, includeSession: true, success: success, failure: failure)
    }
    
    static func request(method:RequestMethod, url: URL, parameters: [String:Any]?, contentType:RequestConttentType, includeSession:Bool, success: @escaping (Data) -> (Void), failure: @escaping (ModelError) -> (Void))
    {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        if contentType == .JSON
        {
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            if let parameters = self.setBaseParameters(parameters: parameters)
            {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
                    request.httpBody = jsonData
                    
                }catch {
                    print(error.localizedDescription)
                }
                
            }
            
        }else
        {
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            if let parameters = self.setBaseParameters(parameters: parameters)
            {
                var parametersString = ""
                
                for (key, value) in parameters
                {
                    var encodedValue = (value as AnyObject).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                    
                    encodedValue = encodedValue?.replacingOccurrences(of: "+", with: "%2B")
                    
                    parametersString = parametersString + key + "=" + encodedValue! + "&"
                }
                
                parametersString = String(parametersString[..<parametersString.index(before: parametersString.endIndex)])
                
                if method == .GET
                {
                    request.url = URL(string: url.absoluteString + "?" + parametersString)!
                    
                }else if method == .POST
                {
                    request.httpBody = parametersString.data(using: .utf8)
                }
            }
        }
        
        if includeSession == true
            
        {
//            request.setValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        
            DispatchQueue.main.async {
                
                if let error = error
                {
                    let bbError = ModelError()
                    bbError.UserFriendlyMessage = error.localizedDescription
                    failure(bbError)
                    
                }else
                {
                    if let response = response as? HTTPURLResponse {
                        
                        if response.statusCode == 401 || response.statusCode == 403
                        {
                            let bbError = ModelError()
                            bbError.Code = "401"
                            
                            failure(bbError)
                            
                        }else
                        {
                            if let data = data
                            {
                                success(data)
                            }else
                            {
                                failure(ModelError())
                            }
                        }
                    }
        
                }
                
            }
        }
        
        task.resume()
    }
    
    static func setBaseParameters(parameters: [String:Any]?) -> [String:Any]? {
        var newParameters = parameters ?? [String:Any]()
        newParameters["language"] = "en-US"
        newParameters["api_key"] = TPObStr.f.d.two.b.zero.four.three.four.two.zero.four.eight.f.a.two.d.five.f.seven.two.eight.five.six.one.eight.six.six.a.d.five.two.a
        return newParameters
    }
}
