//
//  BaseViewController.swift
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            NFX.sharedInstance().show()
        }
    }
    
    func emptyView(collectionView:UICollectionView,items:[Any]?)  {
        if items?.count ?? 0 == 0 {
            let emptyView = EmptyView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 400))
            collectionView.backgroundView = emptyView
        }else {
            collectionView.backgroundView = nil
        }
    }

}
