//  Created by Nihat on 18.09.2018.
//  Copyright © 2018 Nihat Basmacı. All rights reserved.
//

import UIKit

@IBDesignable
class CardView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 10 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = self.cornerRadius
        layer.masksToBounds = true
        
    }
    
}
