//
//  EmpyTableCell.swift
//  Hey
//
//  Created by Nihat on 18.09.2018.
//  Copyright © 2018 Nihat. All rights reserved.
//

import UIKit


class EmptyView: UIView {
    @IBOutlet var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
        
    }
    
    func commonInit()  {
        Bundle.main.loadNibNamed("EmptyView", owner: self, options: nil)
        self.contentView.frame = self.frame
        self.addSubview(self.contentView)
    }
}
