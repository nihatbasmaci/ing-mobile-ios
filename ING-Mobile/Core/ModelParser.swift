//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//

import UIKit

class ModelParser: NSObject
{

    let encoder = JSONEncoder()
    let decoder = JSONDecoder()

    func encode<T:Encodable>(model: T) -> Data?
    {

        if let jsonData = try? self.encoder.encode(model)
        {
            return jsonData
        }
        
        return nil
    }
    
    func decode<T:Decodable>(data:Data, model: T.Type) -> T?
    {
        do{
            let model = try self.decoder.decode(T.self, from: data)
            return model
        }
        catch {
            print("* * * * ** * * * ** * * * ** * * * * JSON PARSE ERROR * * * * * * * * * * * * * * *")
            print(error)
            
        }
        return nil
    }

}
