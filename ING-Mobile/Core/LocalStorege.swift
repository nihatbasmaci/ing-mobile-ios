//
//  UserDefaults.swift
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//

import Foundation
class LocalStorege: NSObject {
    
    static var shared = LocalStorege()
    
    func saveFavorite(object : ResultModel){
        var array : [ResultModel] = []
        if let data = UserDefaults.standard.data(forKey: "SaveFavorite") {
            
            do {
                array = try JSONDecoder().decode([ResultModel].self, from: data)
                let findList = array.filter({$0.id == object.id})
                if findList.count == 0 {
                    array.append(object)
                }
                else {
                    findList.forEach { (item) in
                        if let index = array.firstIndex(where: {$0.id == item.id ?? 0}) {
                            array.remove(at: index)
                        }
                    }
                    
                }
                let data = try? JSONEncoder().encode(array)
                UserDefaults.standard.set(data, forKey: "SaveFavorite")
            } catch  {
                array.append(object)
                let data = try? JSONEncoder().encode(object)
                UserDefaults.standard.set([data], forKey: "SaveFavorite")
            }
            
        }
        else {
            array.append(object)
            let data = try? JSONEncoder().encode(array)
            UserDefaults.standard.set(data, forKey: "SaveFavorite")
        }
    }

    func getFavorites() -> [ResultModel]? {
        if let placeData = UserDefaults.standard.data(forKey: "SaveFavorite") {
            let placeArray = try? JSONDecoder().decode([ResultModel].self, from: placeData)
            return placeArray
        }
        else {
            return nil
        }
        
    }

    func removeFavorite(id:Int) {
        if let placeData = UserDefaults.standard.data(forKey: "SaveFavorite") {
            var placeArray = try? JSONDecoder().decode([ResultModel].self, from: placeData)
            if let index = placeArray?.firstIndex(where: {$0.id == id}) {
                placeArray?.remove(at: index)
            }
            let placesData = try? JSONEncoder().encode(placeArray)
            UserDefaults.standard.set(placesData, forKey: "SaveFavorite")
        }
    }

    func findFavorite(id:Int) -> Bool {
        if let placeData = UserDefaults.standard.data(forKey: "SaveFavorite") {
            let placeArray = try? JSONDecoder().decode([ResultModel].self, from: placeData)
            if let _ = placeArray?.firstIndex(where: {$0.id == id}) {
                return true
            }
        }
        return false
    }

    
}
