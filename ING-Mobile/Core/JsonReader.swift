
import Foundation

class JsonReader {
    
    static func readJson<T: Decodable>(with path: String) -> T? {
        
        if let bundle = Bundle.main.url(forResource: path, withExtension: "json") {
            do {
                let data = try Data(contentsOf: bundle, options: .mappedIfSafe)
                let decodedData = try JSONDecoder().decode(T.self, from: data)
                return decodedData
            } catch {
                fatalError("\(error)")
            }
            
        }
        
        fatalError()
    }
}
