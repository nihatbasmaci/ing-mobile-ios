//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//



import UIKit

enum MovieDetail
{
    // MARK: Use cases
    
    enum FetchOrders
    {
        struct Request
        {
            var movieID: Int?
        }
        class Response : ResultModel
        {

        }
        struct ViewModel
        {
            struct DisplayedMovie
            {
                var title: String
                var description: String
                var image: String
            }
            var displayedMovie: DisplayedMovie
        }
    }
}

