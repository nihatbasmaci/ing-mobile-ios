//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//



import UIKit

@objc protocol MovieDetailRoutingLogic
{
    func routeToShowOrder(segue: UIStoryboardSegue?)
}

protocol MovieDetailDataPassing
{
    var dataStore: MovieDetailDataStore? { get set }
    var movieListDataStore: MovieListDataStore? { get set }
}

class MovieDetailRouter: NSObject, MovieDetailRoutingLogic, MovieDetailDataPassing
{
    
    weak var viewController: MovieDetailViewController?
    var dataStore: MovieDetailDataStore?
    var movieListDataStore: MovieListDataStore?
    
    func routeToShowOrder(segue: UIStoryboardSegue?) { }
   
}
