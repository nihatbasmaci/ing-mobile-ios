//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//



import UIKit

class MovieDetailWorker
{
    
    func fetchOrder(request: MovieDetail.FetchOrders.Request,
                    success: @escaping (MovieDetail.FetchOrders.Response) -> (Void),
                    failure: @escaping (ModelError) -> (Void))
    {
        
        RequestManager.get(url: APILookUp.getDetail(movieID: request.movieID ?? 0), parameters: nil, success: { (data) -> (Void) in
            
            let list = ModelParser().decode(data: data, model: MovieDetail.FetchOrders.Response.self)
            
            if let list = list
            {
                success(list)
                return
            }
            
            failure(ModelError())
            
        }) { (error) -> (Void) in
            
            failure(error)
            
        }
    }
}
