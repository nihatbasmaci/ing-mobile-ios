//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//



import UIKit

protocol MovieDetailPresentationLogic
{
    func presentFetchedOrders(response: MovieDetail.FetchOrders.Response)
}

class MovieDetailPresenter: MovieDetailPresentationLogic
{
    weak var viewController: MovieDetailDisplayLogic?

    func presentFetchedOrders(response: MovieDetail.FetchOrders.Response)
    {
        
        let displayed = MovieDetail.FetchOrders.ViewModel.DisplayedMovie(title: response.title ?? "",
                                                                         description: response.overview ?? "",
                                                                         image: response.posterPath ?? "")
        let viewModel = MovieDetail.FetchOrders.ViewModel.init(displayedMovie: displayed)
        viewController?.displayFetchedOrders(viewModel: viewModel)
    }
}
