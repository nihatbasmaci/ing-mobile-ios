//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//


import UIKit

protocol MovieDetailBusinessLogic
{
    func fetchOrders()
    
}

protocol MovieDetailDataStore
{
    var result: ResultModel? { get }
    var selectedItem: ResultModel? { get set }
}

class MovieDetailInteractor: MovieDetailBusinessLogic, MovieDetailDataStore
{
    
    var presenter: MovieDetailPresentationLogic?
    var ordersWorker = MovieDetailWorker()
    var result: ResultModel?
    var selectedItem: ResultModel?
    
    func fetchOrders()
    {
        var request = MovieDetail.FetchOrders.Request()
        request.movieID = self.selectedItem?.id
        self.ordersWorker.fetchOrder(request: request) { (response) -> (Void) in
            self.result = response
            self.presenter?.presentFetchedOrders(response: response)
        } failure: { (error) -> (Void) in
            
        }
    }
}
