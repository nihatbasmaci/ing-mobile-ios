//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//



import UIKit


protocol MovieDetailDisplayLogic: class
{
    func displayFetchedOrders(viewModel: MovieDetail.FetchOrders.ViewModel)
}

class MovieDetailViewController: BaseViewController, MovieDetailDisplayLogic
{
    
    @IBOutlet weak var patternImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    var interactor: MovieDetailBusinessLogic?
    var router: (NSObjectProtocol & MovieDetailRoutingLogic & MovieDetailDataPassing)?
    var favoriteButtonClicked : (() -> ())?
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = MovieDetailInteractor()
        let presenter = MovieDetailPresenter()
        let router = MovieDetailRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.title = "Movie Detail"
        self.fetchOrders()
        self.refreshFavoriButton()
    }
    
    func fetchOrders()
    {
        interactor?.fetchOrders()
    }
    
    func displayFetchedOrders(viewModel: MovieDetail.FetchOrders.ViewModel)
    {

        self.titleLabel.text = viewModel.displayedMovie.title
        self.descriptionLabel.text = viewModel.displayedMovie.description

        let baseUrl = Manager.shared.configuration.baseImageURL + viewModel.displayedMovie.image
        self.patternImageView.downloaded(from: baseUrl) { () -> (Void) in
            self.imageHeightConstraint.constant = self.view.frame.width * (self.patternImageView.image?.size.height ?? 0) / (self.patternImageView.image?.size.width ?? 0)
            self.view.layoutIfNeeded()
        }
    }
    
    func refreshFavoriButton()  {
        
        if self.router?.dataStore?.selectedItem?.find() ?? false {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem().itemWith(colorfulImage: UIImage(named: "favorite"),
                                                                                target: self, action: #selector(addTapped))
        }
        else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem().itemWith(colorfulImage: UIImage(named: "dotFavorite"),
                                                                                     target: self, action: #selector(addTapped))
        }
    }
    
    @objc func addTapped() {
        self.favoriteButtonClicked?()
        self.refreshFavoriButton()
    }
}
