//
//  MovieFooterView.swift
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//

import UIKit

protocol MovieFooterViewDelegate:class {
    func showMore()
}

class MovieFooterView: UICollectionReusableView {

    weak var delegate : MovieFooterViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    @IBAction func showMoreAction(_ sender: Any) {
        self.delegate?.showMore()
    }
    
}
