//
//  MovieCollectionViewCell.swift
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var patternImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    var data:ResultModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func refreshData(data:ResultModel?) {
        self.data = data
        self.patternImageView.image = nil
        if let url = data?.posterPath {
            let baseUrl = Manager.shared.configuration.baseImageURL + url
            self.patternImageView.downloaded(from: baseUrl,
                                             contentMode: .scaleAspectFill) { () -> (Void) in
                print("Donwloaded")
            }
        }
        
        self.titleLabel.text        = data?.title
        self.descriptionLabel.text  = data?.overview
        self.voteCountLabel.text    = data?.voteAverage?.description
        self.refreshFavoriButton()
    }
    
    func refreshFavoriButton()  {
        if self.data?.find() ?? false {
            self.favoriteButton.setImage(UIImage.init(named: "favorite"), for: .normal)
        }
        else {
            self.favoriteButton.setImage(UIImage.init(named: "dotFavorite"), for: .normal)
        }
    }
    
    @IBAction func addFavoriteAction() {
        guard let object = self.data else { return }
        
        if object.find()
        {
            object.remove()
        }
        else {
            object.save()
        }
        
        self.refreshFavoriButton()
    }
    

}
