//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//


import UIKit

protocol MovieListPresentationLogic
{
    func presentFetchedOrders(response: MovieList.FetchOrders.Response)
}

class MovieListPresenter: MovieListPresentationLogic
{
    weak var viewController: MovieListDisplayLogic?

    func presentFetchedOrders(response: MovieList.FetchOrders.Response)
    {
        let displayedOrders: [MovieList.FetchOrders.ViewModel.DisplayedOrder] = []
        let viewModel = MovieList.FetchOrders.ViewModel(displayedOrders: displayedOrders)
        viewController?.displayFetchedOrders(viewModel: viewModel)
    }
}
