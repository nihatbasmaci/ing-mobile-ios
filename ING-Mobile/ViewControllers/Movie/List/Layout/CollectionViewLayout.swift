//
//  Layout.swift
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//

import Foundation
import UIKit

class CollectionViewListLayout: UICollectionViewFlowLayout {
    override func prepare() {
        super.prepare()
        let width = UIScreen.main.bounds.width
        self.minimumLineSpacing = 0
        self.minimumInteritemSpacing = 0
        self.sectionInset = .zero
        self.itemSize = CGSize(width: (width), height: (UIScreen.main.bounds.height / 1.5))
        self.footerReferenceSize = CGSize(width: (width), height: 50 )
    }
}

class CollectionViewGridLayout: UICollectionViewFlowLayout {
    override func prepare() {
        super.prepare()
        self.minimumLineSpacing = 0
        self.minimumInteritemSpacing = 0
        self.sectionInset = .zero
        let width = UIScreen.main.bounds.width
        self.itemSize = CGSize(width: (width) / 2, height: (width - (width / 4)))
        self.footerReferenceSize = CGSize(width: (width), height: 50 )
    }
}
