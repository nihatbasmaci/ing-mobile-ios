//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//



import UIKit

class MovieListWorker
{
    
    func fetchOrder(request: MovieList.FetchOrders.Request,
                    success: @escaping (MovieList.FetchOrders.Response) -> (Void),
                    failure: @escaping (ModelError) -> (Void))
    {
        
        var parameters : [String:String] = [:]
        parameters["page"] = request.page?.description
        
        RequestManager.get(url: APILookUp.getPopular(), parameters: parameters, success: { (data) -> (Void) in
            
            let list = ModelParser().decode(data: data, model: MovieList.FetchOrders.Response.self)
            
            if let list = list
            {
                success(list)
                return
            }
            
            failure(ModelError())
            
        }) { (error) -> (Void) in
            
            failure(error)
            
        }
    }
}

class MovieListSPY: MovieListWorker {
    
    override func fetchOrder(request: MovieList.FetchOrders.Request,
                             success: @escaping (MovieList.FetchOrders.Response) -> (Void),
                             failure: @escaping (ModelError) -> (Void)) {
        
        if let response : MovieList.FetchOrders.Response = JsonReader.readJson(with: "MovieList") {
            success(response)
        }
        else {
            failure(ModelError())
        }
        
    }
}
