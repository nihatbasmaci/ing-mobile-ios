//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//

import UIKit

enum ListModeType {
    case list
    case grid
}

protocol MovieListDisplayLogic: class
{
    func displayFetchedOrders(viewModel: MovieList.FetchOrders.ViewModel)
}

class MovieListViewController: BaseViewController, MovieListDisplayLogic
{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UITextField!
    
    var interactor: MovieListBusinessLogic?
    var router: (NSObjectProtocol & MovieListRoutingLogic & MovieListDataPassing)?
    var listModeType : ListModeType = .grid
    var loading = false
    lazy var refreshControl = UIRefreshControl()

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = MovieListInteractor()
        let presenter = MovieListPresenter()
        let router = MovieListRouter()
        interactor.ordersWorker = MovieListWorker()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
        self.fetchOrders()
    }
    
    func prepareUI() {
        
        self.collectionView.register(UINib.init(nibName: "MovieCollectionViewCell", bundle: nil),
                                     forCellWithReuseIdentifier: "MovieCollectionViewCell")
        self.collectionView.register(UINib.init(nibName: "MovieFooterView", bundle: nil),
                                     forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                                     withReuseIdentifier: "MovieFooterView")
        
        self.searchBar.delegate = self
        self.searchBar.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        let add = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(addTapped))
        navigationItem.rightBarButtonItems = [add]
    
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.collectionView.addSubview(refreshControl)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.title = "Movie List"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    @objc func addTapped() {
        self.listModeType = self.listModeType == .list ? .grid : .list
        switch listModeType {
        case .list:
            self.collectionView.setCollectionViewLayout(CollectionViewListLayout(), animated: true)
        case .grid:
            self.collectionView.setCollectionViewLayout(CollectionViewGridLayout(), animated: true)
        }
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.router?.dataStore?.filtered?.removeAll()
        self.router?.dataStore?.result?.removeAll()
        self.fetchOrders()
    }
    
    func fetchOrders()
    {
        let request = MovieList.FetchOrders.Request()
        interactor?.fetchOrders(request: request)
    }
    
    func displayFetchedOrders(viewModel: MovieList.FetchOrders.ViewModel)
    {
        self.refreshControl.endRefreshing()
        self.collectionView.reloadData()
    }
}

extension MovieListViewController:UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        self.emptyView(collectionView: collectionView,items: self.router?.dataStore?.result)
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.router?.dataStore?.result?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCollectionViewCell", for: indexPath) as! MovieCollectionViewCell
        cell.refreshData(data: self.router?.dataStore?.result?[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.router?.routeToMoviewDetail(cell: collectionView.cellForItem(at: indexPath) as! MovieCollectionViewCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionFooter:

            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MovieFooterView", for: indexPath) as! MovieFooterView
            footerView.delegate = self
            if self.searchBar.text?.count ?? 0 > 0 {
                footerView.isHidden = true
            }
            else {
                footerView.isHidden = false
            }
            
            return footerView
            
        default:
            fatalError("Unexpected element kind")
        }
    }
}

extension MovieListViewController:MovieFooterViewDelegate,UITextFieldDelegate {
    
    @objc func textDidChange() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload(_:)), object: searchBar)
        perform(#selector(self.reload(_:)), with: searchBar, afterDelay: 0.5)
    }
    
    @objc func reload(_ searchBar: UITextField) {
        if let searchText = self.searchBar.text?.lowercased(with: Locale.init(identifier: "tr_TR")) {
            if searchText.count == 0 {
                let filter = self.router?.dataStore?.filtered
                self.router?.dataStore?.result = filter
                self.collectionView.reloadData()
            } else {
                if self.router?.dataStore?.result?.count ?? 0 == 0 {
                    return
                }

                let searchFilter = self.searchFilter(searchText: searchText)
                self.router?.dataStore?.result = searchFilter
                self.collectionView.reloadData()
            }
        }
    }
    
    func searchFilter(searchText:String) -> [ResultModel] {
        let filter = self.router?.dataStore?.result?.filter {
            return ($0.title?.localizedCaseInsensitiveContains(searchText) ?? false) ||
                   ($0.overview?.localizedCaseInsensitiveContains(searchText) ?? false)
        }
        return filter ?? []
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.reload(textField)
        self.searchBar.resignFirstResponder()
        return true
    }

    
    func showMore() {
        self.router?.dataStore?.page += 1
        let request = MovieList.FetchOrders.Request.init(page: self.router?.dataStore?.page)
        self.interactor?.fetchOrders(request: request)
    }
    
    /* Optional olarak eklendi. (LoadMore)
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let distance = scrollView.contentSize.height - (targetContentOffset.pointee.y + scrollView.bounds.height)
        if distance < 200 {
            self.router?.dataStore?.page += 1
            let request = MovieList.FetchOrders.Request.init(page: self.router?.dataStore?.page)
            self.interactor?.fetchOrders(request: request)
        }
    }
    */
}
