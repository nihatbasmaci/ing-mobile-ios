//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//



import UIKit

protocol MovieListRoutingLogic
{
    func routeToMoviewDetail(cell:MovieCollectionViewCell)
}

protocol MovieListDataPassing
{
    var dataStore: MovieListDataStore? { get set }
}

class MovieListRouter: NSObject, MovieListRoutingLogic, MovieListDataPassing
{

    weak var viewController: MovieListViewController?
    var dataStore: MovieListDataStore?
    
    func routeToMoviewDetail(cell:MovieCollectionViewCell) {
        let vc = UIStoryboard.main.instantiate() as MovieDetailViewController
        vc.router?.dataStore?.selectedItem = cell.data
        vc.favoriteButtonClicked = { [weak cell] () -> Void in
            cell?.addFavoriteAction()
        }
        self.viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
}
