//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//


import UIKit

protocol MovieListBusinessLogic
{
    func fetchOrders(request: MovieList.FetchOrders.Request)
}

protocol MovieListDataStore
{
    var result: [ResultModel]? { get set}
    var filtered: [ResultModel]? { get set}
    var page : Int { get set}
}

class MovieListInteractor: MovieListBusinessLogic, MovieListDataStore
{
    var page: Int = 1
    var presenter: MovieListPresentationLogic?
    
    var ordersWorker : MovieListWorker?
    var result: [ResultModel]?
    var filtered: [ResultModel]?
    
    func fetchOrders(request: MovieList.FetchOrders.Request)
    {
        var request = request
        request.page = self.page
        self.ordersWorker?.fetchOrder(request: request) { (response) -> (Void) in
            self.result = (self.result ?? []) + (response.results ?? [])
            self.filtered = self.result
            self.presenter?.presentFetchedOrders(response: response)
        } failure: { (error) -> (Void) in
            
        }
    }
}
