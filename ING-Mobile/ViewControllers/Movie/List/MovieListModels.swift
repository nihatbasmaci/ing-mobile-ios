//
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//


import UIKit

enum MovieList
{
    // MARK: Use cases
    
    enum FetchOrders
    {
        struct Request
        {
            var page: Int?
        }
        struct Response : Codable
        {
            let page : Int?
            let results : [ResultModel]?
            let total_pages : Int?
            let total_results : Int?
        }
        struct ViewModel
        {
            struct DisplayedOrder
            {
                var id: String
                var date: String
                var email: String
                var name: String
                var total: String
            }
            var displayedOrders: [DisplayedOrder]
        }
    }
}



class ResultModel : Codable {
    
    var adult : Bool?
    var backdropPath : String?
    var genreIds : [Int]?
    var id : Int?
    var originalLanguage : String?
    var originalTitle : String?
    var overview : String?
    var popularity : Float?
    var posterPath : String?
    var releaseDate : String?
    var title : String?
    var video : Bool?
    var voteAverage : Float?
    var voteCount : Int?
    
    var isFavorite : Bool!
    
    init() {
        
    }
    enum CodingKeys: String, CodingKey {
        case adult = "adult"
        case backdropPath = "backdrop_path"
        case genreIds = "genre_ids"
        case id = "id"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview = "overview"
        case popularity = "popularity"
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title = "title"
        case video = "video"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }

    func save() {
        LocalStorege.shared.saveFavorite(object: self)
    }
    
    func remove() {
        if let id = self.id {
            LocalStorege.shared.removeFavorite(id: id)
        }
    }
    
    func find() -> Bool {
        if let id = self.id {
           return LocalStorege.shared.findFavorite(id: id)
        }
        else {
            return false
        }
    }
}
