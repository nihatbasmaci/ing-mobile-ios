//
//  AppDelegate.swift
//  ING-Mobile
//
//  Created by Nihat Basmacı on 11.01.2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        #if TEST
        Manager.shared.configuration.baseURL = "https://api.themoviedb.org/"
        NFX.sharedInstance().start()
        #else
        Manager.shared.configuration.baseURL = "https://api.themoviedb.org/"
        #endif
        
        let navbarApperance = UINavigationBar.appearance()
        navbarApperance.barStyle = .default
        navbarApperance.isTranslucent = false
        navbarApperance.setBackgroundImage(UIImage(), for: .default)
        navbarApperance.barTintColor = UIColor.white
        navbarApperance.tintColor = UIColor.darkGray
        navbarApperance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                                               NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        
        return true
    }

}

