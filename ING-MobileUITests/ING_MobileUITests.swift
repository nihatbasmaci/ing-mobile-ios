//
//  ING_MobileUITests.swift
//  ING-MobileUITests
//
//  Created by Nihat Basmacı on 11.01.2021.
//

import XCTest
@testable import ING_Mobile

class ING_MobileUITests: XCTestCase {

    var app : XCUIApplication!
    override func setUp() {
        super.setUp()
        app = XCUIApplication()
        app.launch()
    }
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    func test_functionality()  {
//        let collectionViewsQuery = app.collectionViews
//        let cell = collectionViewsQuery.children(matching: .cell).element(boundBy: 0)
//        cell.buttons["favorite"].tap()
//        cell.buttons["dotFavorite"].tap()
//
//        let cell2 = collectionViewsQuery.children(matching: .cell).element(boundBy: 1)
//        cell2.buttons["dotFavorite"].tap()
//        cell2.children(matching: .other).element.children(matching: .other).element.tap()
//
//        let movieDetailNavigationBar = app.navigationBars["Movie Detail"]
//        movieDetailNavigationBar.buttons["favorite"].tap()
//        movieDetailNavigationBar.buttons["Movie List"].tap()
                
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    

}
