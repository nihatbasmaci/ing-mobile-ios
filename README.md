
# ING - Movie

## Features

- Mimari olarak CleanSwift kullanıldı. 
- Test ve Production için iki ayrı target oluşturuldu. Flag ile kontrol sağlandı.
- 3rd party kütüphane kullanılmadı.
- Test target'i için "netfox" eklendi. (gelen giden request,response görüntülemek için)
- RequestManager içerisinde URLSession kullanılarak get ve post istekleri için gerekli manager class'ı oluştuurldu.
- ApiKey için Obfuscation oluşturuldu.
- Favori işlemleri için usuerDefaults kullanıldı.
- List ve Gird için collectionViewLayout kullanıldı.
- LoadMore için alternatif eklendi.
- CustomView için xib kullanıldı.
- UI için storyboard kullanıldı. (Projelerdeki genel tercihim her ekran için bir xib)
- Senaryolar için Unit test ekledim. (ING_MobileTests) - UI Test comment mevcut.

- Bu projede kütüphene kullanacak olsaydım;

    pod 'IQKeyboardManagerSwift'
    pod 'SwiftValidator'
    pod 'Kingfisher'
    pod 'IGListKit'  
    
    spec.dependency 'Alamofire'
    spec.dependency 'Moya'
    spec.dependency 'KeychainAccess'
    spec.dependency 'MBProgressHUD'

## Requirements

- iOS 9.0+
- Xcode 11+

[license-image]: https://img.shields.io/badge/License-MIT-blue.svg
[license-url]: LICENSE
