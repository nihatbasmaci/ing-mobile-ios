//
//  ING_MobileTests.swift
//  ING-MobileTests
//
//  Created by Nihat Basmacı on 11.01.2021.
//

import XCTest
@testable import ING_Mobile

class ING_MobileTests: XCTestCase {

    var movieListViewController : MovieListViewController!
    override func setUp() {
        super.setUp()
        movieListViewController = UIStoryboard.main.instantiate() as MovieListViewController
        (movieListViewController.interactor as? MovieListInteractor)?.ordersWorker = MovieListSPY()
        
    }
    
    func test_gridItem() {
        XCTAssertNotNil(movieListViewController, "Movie List Controller must initilazed!")
        movieListViewController.loadViewIfNeeded()
        let cell = movieListViewController.collectionView(movieListViewController.collectionView, cellForItemAt: IndexPath.init(item: 0, section: 0)) as! MovieCollectionViewCell
        XCTAssertEqual(cell.titleLabel.text, "Wonder Woman 1984", "Title label couldn't set!")
    }
    
    func test_search() {
        XCTAssertNotNil(movieListViewController, "Movie List Controller must initilazed!")
        movieListViewController.loadViewIfNeeded()
        let searchText = "Wonder Woman"
        let results = movieListViewController.searchFilter(searchText: searchText)
        XCTAssertGreaterThan(results.count, 0, "Search couldn't work!")
        XCTAssertEqual(results.first?.title, "Wonder Woman 1984", "Search couldn't work!")
    }
    
    func test_lowerAndUpperCase() {
        XCTAssertNotNil(movieListViewController, "Movie List Controller must initilazed!")
        movieListViewController.loadViewIfNeeded()
        let searchText = "WONDER wOmAn"
        let results = movieListViewController.searchFilter(searchText: searchText)
        XCTAssertGreaterThan(results.count, 0, "Search couldn't work!")
    }
    
    func test_AddFavorite() {
        XCTAssertNotNil(movieListViewController, "Movie List Controller must initilazed!")
        movieListViewController.loadViewIfNeeded()
        let cell = movieListViewController.collectionView(movieListViewController.collectionView, cellForItemAt: IndexPath.init(item: 0, section: 0)) as! MovieCollectionViewCell
        cell.data?.save()
        XCTAssertTrue(LocalStorege.shared.findFavorite(id: 464052), "Add favorite couldn't work!")
    }
    
    func test_RemoveFavorite() {
        XCTAssertNotNil(movieListViewController, "Movie List Controller must initilazed!")
        movieListViewController.loadViewIfNeeded()
        let cell = movieListViewController.collectionView(movieListViewController.collectionView, cellForItemAt: IndexPath.init(item: 0, section: 0)) as! MovieCollectionViewCell
        cell.data?.remove()
        XCTAssertFalse(LocalStorege.shared.findFavorite(id: cell.data?.id ?? 0), "Remove favorite couldn't work!")
    }
    
    
    override func tearDown() {
        movieListViewController = nil
        super.tearDown()
    }
}
